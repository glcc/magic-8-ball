//
//  ViewController.swift
//  Magic_8_Ball
//
//  Created by Gerson Costa on 15/06/2018.
//  Copyright © 2018 Gerson Costa. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var ivBall: UIImageView!
    
    var ballImages = ["ball1", "ball2", "ball3", "ball4", "ball5"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }

    @IBAction func guideBtPressed(_ sender: Any) {
        getAnswer()
    }
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        getAnswer()
    }
    
    func getAnswer() {
        ivBall.image = UIImage(named: ballImages[Int(arc4random_uniform(5))])
    }
}

